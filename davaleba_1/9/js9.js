function createTable() {
        var width = document.getElementById('widthInput').value;
        var height = document.getElementById('heightInput').value;
        var backgroundColor = document.getElementById('backgroundColorInput').value;
        var borderSize = document.getElementById('borderSizeInput').value;

        var tableContainer = document.getElementById('tableContainer');
        var tableHTML = '<table style="width: ' + width + 'px; height: ' + height + 'px; background-color: ' + backgroundColor + '; border: ' + borderSize + 'px solid black">';
        tableHTML += '<tr><th>Header 1</th><th>Header 2</th></tr>';
        tableHTML += '<tr><td>Data 1</td><td>Data 2</td></tr></table>';
        tableContainer.innerHTML = tableHTML;
    }
