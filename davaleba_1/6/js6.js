function createTable(width, height) {
        var tableContainer = document.getElementById('tableContainer');
        var tableHTML = '<table><tr><th>Header 1</th><th>Header 2</th></tr>';
        tableHTML += '<tr><td>Data 1</td><td>Data 2</td></tr></table>';
        tableContainer.innerHTML = tableHTML;
        var table = document.querySelector('table');
        table.style.width = width + 'px';
        table.style.height = height + 'px';
    }
