function createTable(borderSize) {
        var tableContainer = document.getElementById('tableContainer');
        var tableHTML = '<table style="border: ' + borderSize + 'px solid black">';
        tableHTML += '<tr><th>Header 1</th><th>Header 2</th></tr>';
        tableHTML += '<tr><td>Data 1</td><td>Data 2</td></tr></table>';
        tableContainer.innerHTML = tableHTML;
    }
