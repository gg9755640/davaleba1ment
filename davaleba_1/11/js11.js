function getFirstTenNumbers() {
        var numbersList = [];
        for (var i = 1; i <= 10; i++) {
            numbersList.push(i);
        }
        return numbersList;
    }

    function displayNumbers() {
        var numbersList = getFirstTenNumbers();
        var numbersListElement = document.getElementById('numbersList');
        numbersListElement.innerHTML = '';
        numbersList.forEach(function(number) {
            var listItem = document.createElement('li');
            listItem.textContent = number;
            numbersListElement.appendChild(listItem);
        });
    }


    displayNumbers();
