function generateTable() {
        var rows = parseInt(document.getElementById('rowsInput').value);
        var cols = parseInt(document.getElementById('colsInput').value);

        var tableContainer = document.getElementById('tableContainer');
        var tableHTML = '<table>';

        for (var i = 0; i < rows; i++) {
            tableHTML += '<tr>';
            for (var j = 0; j < cols; j++) {
                tableHTML += '<td>Row ' + (i + 1) + ', Col ' + (j + 1) + '</td>';
            }
            tableHTML += '</tr>';
        }

        tableHTML += '</table>';
        tableContainer.innerHTML = tableHTML;
    }
