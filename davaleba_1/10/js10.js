function createTable() {
        var width = document.getElementById('widthInput').value + 'px';
        var height = document.getElementById('heightInput').value + 'px';
        var backgroundColor = document.getElementById('backgroundColorInput').value;
        var borderSize = document.getElementById('borderSizeInput').value + 'px';

        var tableContainer = document.getElementById('tableContainer');
        var table = document.createElement('table');
        table.style.width = width;
        table.style.height = height;
        table.style.backgroundColor = backgroundColor;
        table.style.border = 'solid ' + borderSize;

        var row1 = table.insertRow();
        var cell1_1 = row1.insertCell(0);
        var cell1_2 = row1.insertCell(1);
        cell1_1.innerHTML = "Header 1";
        cell1_2.innerHTML = "Header 2";

        var row2 = table.insertRow();
        var cell2_1 = row2.insertCell(0);
        var cell2_2 = row2.insertCell(1);
        cell2_1.innerHTML = "Data 1";
        cell2_2.innerHTML = "Data 2";

        tableContainer.innerHTML = '';
        tableContainer.appendChild(table);
    }
