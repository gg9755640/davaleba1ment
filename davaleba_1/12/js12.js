function displayNaturalNumbers() {
        var numberInput = document.getElementById('numberInput').value;
        var outputElement = document.getElementById('output');
        var numbers = '';

        if (!isNaN(numberInput) && numberInput > 0) {
            for (var i = 1; i <= numberInput; i++) {
                numbers += i + '\n';
            }
        } else {
            numbers = 'Please enter a valid positive number.';
        }

        outputElement.textContent = numbers;
    }
