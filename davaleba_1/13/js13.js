function displayNaturalNumbers() {
        var startInput = document.getElementById('startInput').value;
        var endInput = document.getElementById('endInput').value;
        var outputElement = document.getElementById('output');
        var numbers = '';

        if (!isNaN(startInput) && !isNaN(endInput) && startInput > 0 && endInput > 0 && startInput <= endInput) {
            for (var i = parseInt(startInput); i <= parseInt(endInput); i++) {
                numbers += i + '\n';
            }
        } else {
            numbers = 'Please enter valid positive numbers where start <= end.';
        }

        outputElement.textContent = numbers;
    }
