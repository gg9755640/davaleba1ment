function styleText() {
        var text = document.getElementById('textInput').value;
        var fontSize = document.getElementById('fontSizeInput').value;
        var styledText = document.getElementById('styledText');
        
        styledText.textContent = text;
        styledText.style.fontSize = fontSize + 'px';
    }
