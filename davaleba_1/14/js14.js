function displayNaturalNumbers() {
        var startInput = parseInt(document.getElementById('startInput').value);
        var endInput = parseInt(document.getElementById('endInput').value);
        var outputElement = document.getElementById('output');
        var numbers = '';

        if (!isNaN(startInput) && !isNaN(endInput)) {
            var increment = startInput <= endInput ? 1 : -1;
            for (var i = startInput; i !== endInput + increment; i += increment) {
                numbers += i + '\n';
            }
        } else {
            numbers = 'Please enter valid numbers.';
        }

        outputElement.textContent = numbers;
    }
